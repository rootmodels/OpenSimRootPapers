The simulations for this paper were executed with the version of the source code corresponding to the following git hash: 1d82c5b5de2148c6b95df7b99b54c1eaf03212c1
If you are not familiar with Git, the source code can be downloaded from: https://gitlab.com/rootmodels/OpenSimRoot/-/tree/1d82c5b5de2148c6b95df7b99b54c1eaf03212c1
Once you have downloaded the source code, compile it. The easiest way to do this is by executing the build.sh script found in the top directory by using
bash build.sh
For Windows machines, use
bash build_win64.sh
Note that results may not be identical when run on different systems or when using different compilers.
For this paper, the code was compiled on a Linux machine with GCC6.3.0-2.27.

To create the input files, you will need to run a python script from the OpenSimRootTools repository, which can be found at: https://gitlab.com/rootmodels/opensimroottools
The script in question is called "WriteXMLFromRecipe.py" and can be found in the "Scripts" directory.
You will need to have Python (2 or later) installed with the relevant packages.

Create a directory called "InputFiles" in the directory this file is located.
Open a terminal in this newly created directory.
Run the following commands (this will take a while). Note that you need to use "\" instead of "/" on Windows machines:

python PathToScript/WriteXMLFromRecipe.py ../BarleyScarlettNoRootLoss.xml ../RecipeBarleyNoRootLoss.csv Barley0_0_
python PathToScript/WriteXMLFromRecipe.py ../BarleyScarlettLateral.xml ../RecipeBarleyLateral.csv Barley1_
python PathToScript/WriteXMLFromRecipe.py ../BarleyScarlettAxial.xml ../RecipeBarleyAxial.csv Barley2_
python PathToScript/WriteXMLFromRecipe.py ../BarleyScarlettBoth.xml ../RecipeBarleyBoth.csv Barley3_

python PathToScript/WriteXMLFromRecipe.py ../BeanCariocaNoRootLoss.xml ../RecipeBeanNoRootLoss.csv Bean0_0_
python PathToScript/WriteXMLFromRecipe.py ../BeanCariocaLateral.xml ../RecipeBeanLateral.csv Bean1_
python PathToScript/WriteXMLFromRecipe.py ../BeanCariocaAxial.xml ../RecipeBeanAxial.csv Bean2_
python PathToScript/WriteXMLFromRecipe.py ../BeanCariocaBoth.xml ../RecipeBeanBoth.csv Bean3_

python PathToScript/WriteXMLFromRecipe.py ../MaizeAerenchymaNoRootLoss.xml ../RecipeMaizeNoRootLoss.csv Maize0_0_
python PathToScript/WriteXMLFromRecipe.py ../MaizeAerenchymaLateral.xml ../RecipeMaizeLateral.csv Maize1_
python PathToScript/WriteXMLFromRecipe.py ../MaizeAerenchymaAxial.xml ../RecipeMaizeAxial.csv Maize2_
python PathToScript/WriteXMLFromRecipe.py ../MaizeAerenchymaBoth.xml ../RecipeMaizeBoth.csv Maize3_

Each file will have a name starting with the species.
The first 2 digits will indicate the root loss conditions. The first digit indicates either:
* No root loss (0)
* Lateral root loss (1)
* Axial root loss (2)
* Both lateral and axial root loss (3)
The second digit will indicate the intensity of root loss, if present.
The next 2 digits will indicate the nutrient availability in the soil, of N and P respectively, with 0 indicating low and 1 indicating high availability.
The 5th digit indicates the lateral root branching density, with 0 being low, 1 medium and 2 high.
The 6th digit indicates the number of tillers in barley and the axial (crown in bean and nodal/brace in maize) root number with 0 low, 1 medium and 2 high.
The 7th digit indicates the random number generator seed.