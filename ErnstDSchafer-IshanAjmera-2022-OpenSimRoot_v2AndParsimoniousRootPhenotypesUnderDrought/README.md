The simulations for this paper were executed with the version of the source code corresponding to the following git hash: c87dcec348dc865b37a342da8b6d86e764f82de6
If you are not familiar with Git, the source code can be downloaded from: https://gitlab.com/rootmodels/OpenSimRoot/-/tree/c87dcec348dc865b37a342da8b6d86e764f82de6
Once you have downloaded the source code, compile it. The easiest way to do this is by executing the build.sh script found in the top directory by using
bash build.sh
For Windows machines, use
bash build_win64.sh
Note that results may not be identical when run on different systems or when using different compilers.
For this paper, the code was compiled on a Linux machine with GCC6.3.0-2.27.

To create the input files, you will need to run a python script from the OpenSimRootTools repository, which can be found at: https://gitlab.com/rootmodels/opensimroottools
The script in question is called "WriteXMLFromRecipe.py" and can be found in the "Scripts" directory.
You will need to have Python (3 or later) installed with the relevant packages.

Create a directory called "InputFiles" in the directory this file is located.
Open a terminal in this newly created directory.
Run the following commands (this will take a while). Note that you need to use "\" instead of "/" on Windows machines:

python PathToScript/WriteXMLFromRecipe.py ../MaizeFinal.xml ../RecipeSCD.csv MaizeSCD

Each file will have a name starting with "MaizeSCD".
The first digit will indicate the environmental conditions. These are:
* Iowa rainfed (0)
* Iowa terminal drought (1)
* Zaria rainfed (2)
* Zaria terminal drought (3)
* Jalisco rainfed (4)
* Jalisco terminal drought (5)

The second digit will indicate the root phenotype. These are:
* Reference (0)
* Steep, cheap, deep (1)
* Reference with fewer axial roots (2)
* Reference with steeper axial roots (3)
* Reference with lower lateral root branching density (4)
* Reference with higher root cortical aerenchyma formation (5)
* Reference with fewer and steeper axial roots (6)
* Reference with fewer axial roots and lower lateral root branching density (7)
* Reference with fewer axial roots and higher root cortical aerenchyma formation (8)
* Reference with steeper axial roots and lower lateral root branching density (9)
* Reference with steeper axial roots and higher root cortical aerenchyma formation (10)
* Reference with lower lateral root branching density and higher root cortical aerenchyma formation (11)
* Steep, cheap, deep with more axial roots (12)
* Steep, cheap, deep with shallower axial roots (13)
* Steep, cheap, deep with higher lateral root branching density (14)
* Steep, cheap, deep with lower root cortical aerenchyma formation (15)

The final digit indicates the random number generator seed which has a value between 0 and 4 (inclusive).