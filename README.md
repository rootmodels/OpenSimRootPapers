# OpenSimRootPapers

This is a repository where XML files corresponding to papers with (Open)SimRoot simulations are made publicly accessible.

When adding your XML files to this repository, please follow the following suggestions:

* Make a separate directory whose name includes enough information to identify which paper it belongs to (first author name, year, title/subject).
* Make sure this directory includes a readme file that lists the git commit hash of the code you used AND instruction on how to reproduce all the input files (see below).
* Do not add every separate XML file. Instead, add the minimum number of base files needed and all the instructions/files needed to reproduce all of them.
* Please do not add scripts to this repository, instead add these to the https://gitlab.com/rootmodels/opensimroottools repository.